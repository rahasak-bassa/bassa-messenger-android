package com.rahasak.bix.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.rahasak.bix.R;
import com.rahasak.bix.pojo.Account;
import com.rahasak.bix.util.PreferenceUtil;

public class ConnectIdentityFragment extends BaseFragment {

    private static final String TAG = ConnectIdentityFragment.class.getName();

    private TextView message;

    private Typeface typeface;
    private Account userAccount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_qr_identity_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initPrefs();
        initUi();
        initQrCodeContent();
    }

    private void initPrefs() {
        userAccount = PreferenceUtil.getAccount(getActivity());
    }

    private void initUi() {
        message = getActivity().findViewById(R.id.message);
        message.setTypeface(typeface, Typeface.BOLD);
    }

    private void initQrCodeContent() {
        String did = PreferenceUtil.get(getActivity(), PreferenceUtil.DID);
        String owner = PreferenceUtil.get(getActivity(), PreferenceUtil.OWNER);
        String qrtext = did + ":" +
                owner + ":" +
                "salt" + ":" +
                "signature";
        generateQrCode(qrtext);
    }

    private void generateQrCode(String qrCodeContent) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(qrCodeContent, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }

            ((ImageView) getActivity().findViewById(R.id.qr_code)).setImageBitmap(bmp);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

}
