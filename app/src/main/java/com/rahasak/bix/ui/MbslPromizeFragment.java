package com.rahasak.bix.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rahasak.bix.R;
import com.rahasak.bix.enums.CustomerActionType;

public class MbslPromizeFragment extends Fragment {

    private TextView emptyText;

    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mbsl_promize_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        ((TextView) view.findViewById(R.id.send_promize_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) view.findViewById(R.id.send_promize_desc)).setTypeface(typeface, Typeface.NORMAL);
        ((TextView) view.findViewById(R.id.receive_promize_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) view.findViewById(R.id.receive_promize_desc)).setTypeface(typeface, Typeface.NORMAL);
        ((TextView) view.findViewById(R.id.pay_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) view.findViewById(R.id.pay_desc)).setTypeface(typeface, Typeface.NORMAL);

        view.findViewById(R.id.send_promize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SendPromizeActivity.class);
                intent.putExtra("ACTION", CustomerActionType.CUSTOMER_LIST.toString());
                startActivity(intent);
            }
        });

        view.findViewById(R.id.receive_promize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ReceivePromizeScanActivity.class);
                intent.putExtra("ACTION", CustomerActionType.CUSTOMER_LIST.toString());
                startActivity(intent);
            }
        });

        view.findViewById(R.id.pay_promize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WalletScanQrCodeActivity.class);
                intent.putExtra("ACTION", CustomerActionType.CUSTOMER_LIST.toString());
                startActivity(intent);
            }
        });
        return view;
    }

}
