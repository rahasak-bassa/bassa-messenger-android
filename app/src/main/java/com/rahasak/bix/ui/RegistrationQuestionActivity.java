package com.rahasak.bix.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.bix.R;
import com.rahasak.bix.application.SenzApplication;
import com.rahasak.bix.async.AccountContractExecutor;
import com.rahasak.bix.interfaces.IContractExecutorListener;
import com.rahasak.bix.pojo.Account;
import com.rahasak.bix.pojo.Identity;
import com.rahasak.bix.pojo.Response;
import com.rahasak.bix.pojo.StatusReply;
import com.rahasak.bix.util.ActivityUtil;
import com.rahasak.bix.util.JsonUtil;
import com.rahasak.bix.util.NetworkUtil;
import com.rahasak.bix.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class RegistrationQuestionActivity extends BaseActivity implements IContractExecutorListener {

    // UI fields
    private TextView question1Text;
    private TextView question2Text;
    private TextView question3Text;
    private EditText question1;
    private EditText question2;
    private EditText question3;

    private Identity identity;
    private String answer1;
    private String answer2;
    private String answer3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_question_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    private void initUi() {
        question1Text = (TextView) findViewById(R.id.question1_text);
        question2Text = (TextView) findViewById(R.id.question2_text);
        question3Text = (TextView) findViewById(R.id.question3_text);
        question1 = (EditText) findViewById(R.id.question1);
        question2 = (EditText) findViewById(R.id.question2);
        question3 = (EditText) findViewById(R.id.question3);

        question1Text.setTypeface(typeface, Typeface.NORMAL);
        question2Text.setTypeface(typeface, Typeface.NORMAL);
        question3Text.setTypeface(typeface, Typeface.NORMAL);
        question1.setTypeface(typeface, Typeface.NORMAL);
        question2.setTypeface(typeface, Typeface.NORMAL);
        question3.setTypeface(typeface, Typeface.NORMAL);

        Button yes = (Button) findViewById(R.id.register_btn);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSave();
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Answer questions");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void onClickSave() {
        // validate input
        answer1 = question1.getText().toString().trim().toLowerCase();
        answer2 = question2.getText().toString().trim().toLowerCase();
        answer3 = question3.getText().toString().trim().toLowerCase();

        if (answer1.isEmpty() || answer2.isEmpty() || answer3.isEmpty()) {
            displayInformationMessageDialog("Error", "You need to answer for all questions to complete the registration.");
        } else {
            ActivityUtil.hideSoftKeyboard(this);
            if (NetworkUtil.isAvailableNetwork(RegistrationQuestionActivity.this)) {
                //doUpdate(account, answer1, answer2, answer3);
                saveIdentity();
                navigateToHome();
            } else {
                Toast.makeText(RegistrationQuestionActivity.this, "No network connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void doUpdate(Account account, String answer1, String answer2, String answer3) {
        try {
            HashMap<String, String> createMap = new HashMap<>();
            createMap.put("messageType", "updateAnswers");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("answer1", answer1);
            createMap.put("answer2", answer2);
            createMap.put("answer3", answer3);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.ACCOUNT_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    Toast.makeText(this, "Successfully saved answers", Toast.LENGTH_LONG).show();
                    saveIdentity();
                    navigateToHome();
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while request.");
        }
    }

    private void navigateToSaltConfirmInfo() {
        Intent intent = new Intent(this, SaltConfirmInfoActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

    private void saveIdentity() {
        // save answers
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.QUESTION1, answer1);
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.QUESTION2, answer2);
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.QUESTION3, answer3);
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.DID, identity.getDid());
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.ACCOUNT_NAME, identity.getName());
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.PASSWORD, identity.getPassword());
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.ACCOUNT_PHONE, identity.getPhone());
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.BLOB, identity.getBlob());
        PreferenceUtil.put(RegistrationQuestionActivity.this, PreferenceUtil.EMAIL, identity.getEmail());
    }

    private void navigateToHome() {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

}

