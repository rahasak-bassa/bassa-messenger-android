package com.rahasak.bix.ui;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.bix.R;
import com.rahasak.bix.application.SenzApplication;
import com.rahasak.bix.async.AccountContractExecutor;
import com.rahasak.bix.exceptions.InvalidInputFieldsException;
import com.rahasak.bix.interfaces.IContractExecutorListener;
import com.rahasak.bix.pojo.Response;
import com.rahasak.bix.pojo.StatusReply;
import com.rahasak.bix.pojo.TokenReply;
import com.rahasak.bix.util.ActivityUtil;
import com.rahasak.bix.util.BixUtil;
import com.rahasak.bix.util.CryptoUtil;
import com.rahasak.bix.util.JsonUtil;
import com.rahasak.bix.util.NetworkUtil;
import com.rahasak.bix.util.PreferenceUtil;
import com.rahasaklabs.messanger.spec.MessageServiceGrpc;
import com.rahasaklabs.messanger.spec.MessangerProto;

import org.json.JSONException;
import org.spongycastle.cms.CMSException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class LoginActivity extends BaseActivity implements IContractExecutorListener {

    // UI fields
    private EditText editTextAccount;
    private EditText editTextPassword;
    private Button loginButton;
    private TextView link1;
    private TextView link2;

    // account
    private String did;
    private String owner;
    private String password;

    private static final String TAG = RegistrationActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        initPrefs();
        initUi();
        //initAccount();
        initToolbar();
        initActionBar();

        if (!running) {
            // reg
            new SenzCom().start();
        }
    }

    private void initPrefs() {
        did = PreferenceUtil.get(this, PreferenceUtil.DID);
        owner = PreferenceUtil.get(this, PreferenceUtil.OWNER);
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Login");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        editTextAccount = (EditText) findViewById(R.id.login_account_no);
        editTextPassword = (EditText) findViewById(R.id.login_password);
        loginButton = (Button) findViewById(R.id.login_btn);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickBixLogin();
            }
        });

        editTextAccount.setTypeface(typeface, Typeface.NORMAL);
        editTextPassword.setTypeface(typeface, Typeface.NORMAL);
        loginButton.setTypeface(typeface, Typeface.BOLD);

        link1 = (TextView) findViewById(R.id.register);
        link1.setPaintFlags(link1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        link1.setTypeface(typeface, Typeface.BOLD);
        link1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (did.isEmpty()) {
                    navigateToRegister();
                } else {
                    //navigateForgotPasswordInfo();
                    navigateToRegister();
                }
            }
        });

        link2 = (TextView) findViewById(R.id.forgot_password);
        link2.setPaintFlags(link2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        link2.setTypeface(typeface, Typeface.BOLD);
        link2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navigateForgotPasswordInfo();
                onClickBixPartnerRegister();
                //proto();
            }
        });
    }

    private void initAccount() {
        if (did.isEmpty()) {
            // no registered user for this device
            // enable edit
            editTextAccount.setFocusable(true);
            editTextAccount.setFocusableInTouchMode(true);
            editTextAccount.setEnabled(true);

            // enable register, forgot password link
            link1.setText("Don't have an account, register?");
            link2.setText("Forgot password?");
            link2.setVisibility(View.VISIBLE);
        } else {
            // have registered user for this device
            editTextAccount.setText(did);
//            StringBuilder buf = new StringBuilder(did);
//            String star = new String(new char[did.length() - 4]).replace("\0", "*");
//            buf.replace(2, did.length() - 2, star);
//            editTextAccount.setText(buf.toString());

            // disable edit
            editTextAccount.setFocusable(false);
            editTextAccount.setFocusableInTouchMode(false);
            editTextAccount.setEnabled(false);

            // disable register link
            link1.setText("Forgot password?");
            link2.setVisibility(View.GONE);
        }
    }

    private void onClickLogin() {
        ActivityUtil.hideSoftKeyboard(this);

        try {
            // validate given credentials
            String givenNic = editTextAccount.getText().toString().toUpperCase().trim();
            String givenPassword = editTextPassword.getText().toString().trim();
            ActivityUtil.isValidLoginFields(givenNic, givenPassword);

            // set accountId and hash 256 password
            did = givenNic;
            password = CryptoUtil.hashSha256(givenPassword);

            // send request if network available
            if (NetworkUtil.isAvailableNetwork(this)) {
                doLogin();
            } else {
                Toast.makeText(this, "No network connection", Toast.LENGTH_LONG).show();
            }
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Please enter NIC and Password to log in");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void onClickBixLogin() {
        try {
            X509Certificate certificate = CryptoUtil.getCert("");
            String encData = CryptoUtil.pkcs7Encrypt(certificate, "");
            String orgData = CryptoUtil.pkcs7Decrypt(CryptoUtil.getPrivateKey(this), encData);
            String bixIdentityObj = BixUtil.getBixIdentityObject("eranga@opa.com", "0775432015", "1057208501", encData);
            String req = BixUtil.getAppendLedgerRequest(bixIdentityObj);
            write(req);
            //System.out.println("---" + encData);
            //System.out.println("---" + orgData);
            //System.out.println("---" + req);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (CMSException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void onClickBixPartnerRegister() {
        String req = BixUtil.getPartnerRegisterRequest("eranga@opa.com", "sead.muftic@setecs.com");
        write(req);
    }

    private void proto() {
        ManagedChannel managedChannel = ManagedChannelBuilder.forAddress("10.252.94.210", 9000).usePlaintext().build();
        MessangerProto.Message message = MessangerProto.Message.newBuilder()
                .setId("011")
                .setBody("hoooo")
                .build();

        MessageServiceGrpc.MessageServiceStub asycStub = MessageServiceGrpc.newStub(managedChannel);

        StreamObserver reqObs = new StreamObserver<MessangerProto.Message>() {
            @Override
            public void onNext(MessangerProto.Message value) {

            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {

            }
        };

        StreamObserver respObs = new StreamObserver<MessangerProto.Message>() {
            @Override
            public void onNext(MessangerProto.Message value) {
                System.out.println(value.getBody());
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {

            }
        };

        //StreamObserver rObs = asycStub.streamMessage(respObs);
        //rObs.onNext(message);

        asycStub.serveMessage(message, respObs);

        MessangerProto.Reply reply = MessageServiceGrpc.newBlockingStub(managedChannel).sendMessage(message);
        System.out.println(reply);
    }

    private void doLogin() {
        try {
            HashMap<String, String> createMap = new HashMap<>();
            createMap.put("messageType", "connect");
            createMap.put("execer", did);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("did", did);
            createMap.put("owner", owner);
            createMap.put("password", password);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.ACCOUNT_API, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDeviceToken() {
        try {
            String did = PreferenceUtil.get(this, PreferenceUtil.DID);
            String owner = PreferenceUtil.get(this, PreferenceUtil.OWNER);
            HashMap<String, String> createMap = new HashMap<>();
            createMap.put("messageType", "updateDevice");
            createMap.put("execer", did);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("did", did);
            createMap.put("owner", owner);
            createMap.put("deviceType", "android");
            createMap.put("deviceToken", PreferenceUtil.get(this, PreferenceUtil.FIREBASE_TOKEN));

            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.ACCOUNT_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                if (response.getStatus() == 200) {
                    if (response.getPayload().contains("Updated")) {
                        // device token updated means full login success
                        ActivityUtil.cancelProgressDialog();
                        Toast.makeText(this, "Login success", Toast.LENGTH_LONG).show();

                        PreferenceUtil.put(this, PreferenceUtil.UPDATE_FIREBASE_TOKEN, "no");
                        navigateToHome();
                    } else {
                        // this should be login auth token reply
                        TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
                        if (tokenReply.getStatus() == 200) {
                            loginDone(tokenReply);

                            // update device token
                            if (PreferenceUtil.get(this, PreferenceUtil.UPDATE_FIREBASE_TOKEN).equalsIgnoreCase("yes")) {
                                updateDeviceToken();
                            } else {
                                ActivityUtil.cancelProgressDialog();
                                navigateToHome();
                            }
                        } else if (tokenReply.getStatus() == 402) {
                            // means account not activated
                            ActivityUtil.cancelProgressDialog();
                            //displayInformationMessageDialog("Error", "Your account is not activated, please try to reset password and activate the account.");
                            displayInformationMessageDialog("Error", "Your account is not activated.");
                        } else if (tokenReply.getStatus() == 405) {
                            // means account answers needs to be updated
                            navigateToQuestionInfo();
                        } else {
                            ActivityUtil.cancelProgressDialog();
                            displayInformationMessageDialog("Error", "Something went wrong while login.");
                        }
                    }
                } else {
                    ActivityUtil.cancelProgressDialog();
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while login.");
        }
    }

    private void activateAccount(TokenReply tokenReply) {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_ID, did);
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PASSWORD, password);
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_STATE, "PENDING");
        navigateToSaltConfirmInfo();
    }

    private void loginDone(TokenReply tokenReply) {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        // save prefs
        PreferenceUtil.put(this, PreferenceUtil.DID, did);
        PreferenceUtil.put(this, PreferenceUtil.OWNER, owner);
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PASSWORD, password);
        PreferenceUtil.put(this, PreferenceUtil.TOKEN, tokenReply.getToken());
    }

    private void deviceTokenUpdateDone() {
        // navigate based on questions
        if (PreferenceUtil.get(this, PreferenceUtil.QUESTION1).isEmpty()) {
            navigateToQuestionInfo();
        } else {
            navigateToHome();
        }
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void navigateToRegister() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
        finish();
    }

    private void navigateToSaltConfirmInfo() {
        Intent intent = new Intent(this, SaltConfirmInfoActivity.class);
        this.startActivity(intent);
        this.finish();
    }

    public void navigateToQuestionInfo() {
        Intent intent = new Intent(this, RegistrationQuestionInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void navigateForgotPasswordInfo() {
        Intent intent = new Intent(this, PasswordResetInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public static final String SENZ_HOST = "96.255.175.56";
    public static final int SENZ_PORT = 23648;

    // senz socket
    private Socket socket;
    private DataInputStream inStream;
    private DataOutputStream outStream;
    private BufferedReader input;

    // comm running
    private boolean running;

    void write(String msg) {
        try {
            //  sends the message to the server
            if (socket != null) {
                Log.e(TAG, "write message --- " + msg);
                outStream.writeBytes(msg + "\n\r");
                outStream.flush();
            } else {
                Log.e(TAG, "Socket disconnected");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SenzCom extends Thread {
        @Override
        public void run() {
            running = true;

            try {
                initCom();
                readCom();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeCom();
            }
        }

        private void initCom() throws IOException {
            Log.d(TAG, "init socket");

            socket = new Socket(InetAddress.getByName(SENZ_HOST), SENZ_PORT);
            inStream = new DataInputStream(socket.getInputStream());
            outStream = new DataOutputStream(socket.getOutputStream());
            //input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }

        private void readCom() throws IOException {
            Log.d(TAG, "read socket");

//            StringBuilder builder = new StringBuilder();
//            while (true) {
//                final String message = input.readLine();
//                if (message != null) {
//                    builder.append(message);
//                    Log.d(TAG, "response " + message);
//                } else {
//                    Log.d(TAG, "final response " + builder.toString());
//                }
//            }

            int c;
            StringBuilder raw = new StringBuilder();
            do {
                c = inStream.read();
                raw.append((char) c);
            } while (inStream.available() > 0);
            String resp = raw.toString();
            System.out.println("/////" + resp);
        }

        private void closeCom() {
            running = false;

            try {
                if (socket != null) {
                    socket.close();
                    inStream.close();
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}

