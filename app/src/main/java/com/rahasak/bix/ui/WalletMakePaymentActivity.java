package com.rahasak.bix.ui;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.bix.R;
import com.rahasak.bix.application.SenzApplication;
import com.rahasak.bix.async.AccountContractExecutor;
import com.rahasak.bix.exceptions.InvalidInputFieldsException;
import com.rahasak.bix.interfaces.IContractExecutorListener;
import com.rahasak.bix.pojo.Identity;
import com.rahasak.bix.pojo.Response;
import com.rahasak.bix.pojo.StatusReply;
import com.rahasak.bix.pojo.TokenReply;
import com.rahasak.bix.util.ActivityUtil;
import com.rahasak.bix.util.JsonUtil;
import com.rahasak.bix.util.NetworkUtil;
import com.rahasak.bix.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class WalletMakePaymentActivity extends BaseActivity implements IContractExecutorListener {

    // ui controls
    private Button doneButton;
    private EditText editBixId;
    private EditText editTextAmount;
    private TextView termsText;
    private TextView termsLink;

    private Identity identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_make_payment_layout);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Make Payment");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editBixId = (EditText) findViewById(R.id.bix_id);
        editTextAmount = (EditText) findViewById(R.id.amount);
        termsText = (TextView) findViewById(R.id.terms_text);
        termsLink = (TextView) findViewById(R.id.terms_link);
        termsLink.setPaintFlags(termsLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        editBixId.setTypeface(typeface, Typeface.NORMAL);
        editTextAmount.setTypeface(typeface, Typeface.NORMAL);
        termsText.setTypeface(typeface, Typeface.BOLD);
        termsLink.setTypeface(typeface, Typeface.BOLD);

        LinearLayout scan = findViewById(R.id.scan_bix_id);
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WalletMakePaymentActivity.this, WalletScanQrCodeActivity.class);
                startActivity(intent);
            }
        });

        Button doneButton = (Button) findViewById(R.id.done);
        doneButton.setTypeface(typeface, Typeface.BOLD);
        doneButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickDone();
            }
        });

        termsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTerms();
            }
        });
    }

    private void onClickDone() {
        ActivityUtil.hideSoftKeyboard(this);

        // crate account
        final String bixId = editBixId.getText().toString().trim();
        final String amount = editTextAmount.getText().toString().trim();

        try {
            ActivityUtil.validatePayment(bixId, amount);

            displayConfirmationMessageDialog("Confirm payment", "Please confirm payment " + amount + " to " + bixId, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.isAvailableNetwork(WalletMakePaymentActivity.this)) {
                        //doReg(identity);
                    } else {
                        Toast.makeText(WalletMakePaymentActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the payment.");
        }
    }

    private void doReg(Identity identity) {
        try {
            HashMap<String, String> createMap = new HashMap<>();
            createMap.put("messageType", "create");
            createMap.put("execer", identity.getDid() + ":" + identity.getOwner());
            createMap.put("id", identity.getDid() + System.currentTimeMillis());
            createMap.put("did", identity.getDid());
            createMap.put("owner", identity.getOwner());
            createMap.put("password", identity.getPassword());
            createMap.put("typ", "user");
            createMap.put("pubKey", "pubkey");
            createMap.put("deviceImei", "");
            createMap.put("deviceType", "android");
            createMap.put("deviceToken", PreferenceUtil.get(this, PreferenceUtil.FIREBASE_TOKEN));
            createMap.put("nic", identity.getNic());
            createMap.put("name", identity.getName());
            createMap.put("dob", identity.getDob());
            createMap.put("phone", identity.getPhone());
            createMap.put("email", identity.getEmail());
            createMap.put("taxNo", identity.getTaxNo());
            createMap.put("address", identity.getAddress());
            createMap.put("blob", identity.getBlob());
            createMap.put("employeeName", identity.getEmployeeName());
            createMap.put("occupation", identity.getOccupation());
            createMap.put("employeeAddress", identity.getEmployeeAddress());

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.ACCOUNT_API, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void navigateToTerms() {
        Intent intent = new Intent(WalletMakePaymentActivity.this, TermsOfUseActivity.class);
        WalletMakePaymentActivity.this.startActivity(intent);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                if (response.getStatus() == 201) {
                    TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
                    Toast.makeText(this, "Your account is registered successfully", Toast.LENGTH_LONG).show();
                    registrationDone(tokenReply);
                } else {
                    ActivityUtil.cancelProgressDialog();
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while registering.");
        }
    }

    private void regDone() {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_ID, identity.getDid());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PASSWORD, identity.getPassword());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NIC, identity.getNic());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NAME, identity.getName());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PHONE, identity.getPhone());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NO, identity.getEmail());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_STATE, "PENDING");
        //navigateToSaltConfirmInfo();
    }

    private void navigateToSaltConfirmInfo(String token) {
        Intent intent = new Intent(WalletMakePaymentActivity.this, SaltConfirmInfoActivity.class);
        intent.putExtra("IDENTITY", identity);
        intent.putExtra("TOKEN", token);
        WalletMakePaymentActivity.this.startActivity(intent);
        WalletMakePaymentActivity.this.finish();
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

    private void registrationDone(TokenReply tokenReply) {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        // save prefs
        PreferenceUtil.put(this, PreferenceUtil.DID, identity.getDid());
        PreferenceUtil.put(this, PreferenceUtil.OWNER, identity.getOwner());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PASSWORD, identity.getPassword());
        PreferenceUtil.put(this, PreferenceUtil.TOKEN, tokenReply.getToken());

        navigateToHome();
    }

    private void navigateToHome() {
        Intent intent = new Intent(WalletMakePaymentActivity.this, HomeActivity.class);
        WalletMakePaymentActivity.this.startActivity(intent);
        WalletMakePaymentActivity.this.finishAffinity();
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}
