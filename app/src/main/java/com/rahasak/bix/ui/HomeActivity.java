package com.rahasak.bix.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.bix.R;
import com.rahasak.bix.application.IntentProvider;
import com.rahasak.bix.application.SenzApplication;
import com.rahasak.bix.async.AccountContractExecutor;
import com.rahasak.bix.interfaces.IContractExecutorListener;
import com.rahasak.bix.pojo.ConnectNotificationMessage;
import com.rahasak.bix.pojo.Response;
import com.rahasak.bix.pojo.StatusReply;
import com.rahasak.bix.util.ActivityUtil;
import com.rahasak.bix.util.JsonUtil;
import com.rahasak.bix.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;


public class HomeActivity extends BaseActivity implements IContractExecutorListener {

    private TextView titleTextView;

    private BroadcastReceiver msgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("NOTIFICATION_MESSAGE")) {
                ConnectNotificationMessage msg = intent.getExtras().getParcelable("NOTIFICATION_MESSAGE");
                handleNotificationMessage(msg);
            }
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.home_layout);
        initToolbar();
        initActionBar();
        initBottomFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!SenzApplication.isLogin()) {
            displayInformationMessageDialogConfirm("Session timeout", "Your session has expired, please login again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.stay_in);
                    finishAffinity();
                }
            });
        } else {
            registerReceiver(msgReceiver, new IntentFilter(IntentProvider.ACTION_SENZ));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (msgReceiver != null) unregisterReceiver(msgReceiver);
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        titleTextView = (TextView) findViewById(R.id.title);
        titleTextView.setTypeface(typeface, Typeface.BOLD);
        titleTextView.setText("Bixcoin");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initBottomFragment() {
        // first show wallet
        changeFragment(new WalletBixcoinListFragment(), WalletBixcoinListFragment.class.getSimpleName());

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigation_offers:
                                titleTextView.setText("Bixcoin");
                                changeFragment(new WalletBixcoinListFragment(), WalletBixcoinListFragment.class.getSimpleName());
                                break;
                            case R.id.navigation_promize:
                                titleTextView.setText("Bitcoin");
                                changeFragment(new WalletBitcoinListFragment(), WalletBitcoinListFragment.class.getSimpleName());
                                break;
                            case R.id.navigation_fund_transfer:
                                titleTextView.setText("Bank Cards");
                                changeFragment(new WalletBankCardsListFragment(), WalletBankCardsListFragment.class.getSimpleName());
                                break;
                            case R.id.navigation_payment:
                                titleTextView.setText("Payments");
                                break;
                            case R.id.navigation_settings:
                                titleTextView.setText("Preference");
                                changeFragment(new WalletPreferenceFragment(), WalletPreferenceFragment.class.getSimpleName());
                                break;
                        }
                        return true;
                    }
                });
    }

    public void changeFragment(Fragment fragment, String tagFragmentName) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frame_layout, fragmentTemp, tagFragmentName);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }


    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    Toast.makeText(this, "Permissions updated", Toast.LENGTH_LONG).show();

                    // reload consent fragment
                    SenzApplication.setRefreshConsents(true);
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while updating permissions.");
        }
    }

    @Override
    public void onBackPressed() {
        displayConfirmationMessageDialog("Logout", "Are you sure to exit Bix Wallet", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SenzApplication.setLogin(false);
                SenzApplication.cancelLogoutTimer();

                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.stay_in);
                finishAffinity();
            }
        });
    }

    private void handleNotificationMessage(final ConnectNotificationMessage notificationMessage) {
        ActivityUtil.cancelProgressDialog();

        String msg = notificationMessage.getTracerName() + " wants to access your account details? Do you want to allow access ?";
        displayConfirmationMessageDialog("Allow access", msg, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // request to fetch consents
                allowPermission(notificationMessage);
            }
        });
    }

    private void allowPermission(ConnectNotificationMessage notificationMessage) {
        try {
            String did = PreferenceUtil.get(this, PreferenceUtil.DID);
            String owner = PreferenceUtil.get(this, PreferenceUtil.OWNER);

            HashMap<String, String> createMap = new HashMap<>();
            createMap.put("messageType", "traceConfirm");
            createMap.put("execer", did);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("traceId", notificationMessage.getTraceId());
            createMap.put("accountDid", did);
            createMap.put("accountOwnerDid", owner);
            createMap.put("tracerDid", notificationMessage.getTracerDid());
            createMap.put("tracerOwnerDid", notificationMessage.getTracerDid());
            createMap.put("salt", notificationMessage.getSalt());

            ActivityUtil.showProgressDialog(this, "Updating permission...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRACE_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
