package com.rahasak.bix.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.bix.R;
import com.rahasak.bix.exceptions.InvalidInputFieldsException;
import com.rahasak.bix.exceptions.InvalidPasswordException;
import com.rahasak.bix.exceptions.InvalidPhoneNumberException;
import com.rahasak.bix.exceptions.MisMatchFieldException;
import com.rahasak.bix.pojo.Identity;
import com.rahasak.bix.util.ActivityUtil;
import com.rahasak.bix.util.BixUtil;
import com.rahasak.bix.util.CryptoUtil;
import com.rahasak.bix.util.PreferenceUtil;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class RegistrationActivity extends BaseActivity {

    // ui controls
    private EditText editTextName;
    private EditText editTextEmail;
    private EditText editTextPhone;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button nextButton;

    private TextView termsText;
    private TextView termsLink;

    private Identity identity;
    private String bixMessage;

    private String email;
    private String phone;
    private String bixid;

    private static final String TAG = RegistrationActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_layout);

        initUi();
        initToolbar();
        initActionBar();

        if (!running) {
            // reg
            new SenzCom().start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Register");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextName = (EditText) findViewById(R.id.registering_name);
        editTextEmail = (EditText) findViewById(R.id.registering_email);
        editTextPhone = (EditText) findViewById(R.id.registering_phone_no);
        editTextPassword = (EditText) findViewById(R.id.registering_password);
        editTextConfirmPassword = (EditText) findViewById(R.id.registering_confirm_password);

        editTextName.setTypeface(typeface, Typeface.NORMAL);
        editTextEmail.setTypeface(typeface, Typeface.NORMAL);
        editTextPhone.setTypeface(typeface, Typeface.NORMAL);
        editTextPassword.setTypeface(typeface, Typeface.NORMAL);
        editTextConfirmPassword.setTypeface(typeface, Typeface.NORMAL);

        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setTypeface(typeface, Typeface.BOLD);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //onClickNext();
                onClickRegister();
            }
        });

        termsText = (TextView) findViewById(R.id.terms_text);
        termsLink = (TextView) findViewById(R.id.terms_link);
        termsText.setTypeface(typeface, Typeface.BOLD);
        termsLink.setTypeface(typeface, Typeface.BOLD);
        termsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTerms();
            }
        });
    }

    private void onClickNext() {
        ActivityUtil.hideSoftKeyboard(this);

        final String name = editTextName.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        final String phn = editTextPhone.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim().toUpperCase();
        final String confirmPassword = editTextConfirmPassword.getText().toString().trim().toUpperCase();

        try {
            ActivityUtil.isValidRegistrationFields(name, phn, email, password, confirmPassword);
            String formattedPhone = "+94" + phn.substring(1);

            // save hashed password instead plain password
            final String passwordHash = CryptoUtil.hashSha256(password);

            identity = new Identity();
            identity.setDid(email);
            identity.setOwner("");
            identity.setName(name);
            identity.setPhone(formattedPhone);
            identity.setDob("");
            identity.setEmail(email);
            identity.setPassword(passwordHash);

            navigateNext(identity);
        } catch (InvalidPhoneNumberException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid phone no. Phone no should contain 10 digits and start with 0");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the registration.");
        } catch (MisMatchFieldException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Your password is not matched with the confirm password");
        } catch (InvalidPasswordException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid password. Password should contain minimum 7 characters with special character");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void onClickRegister() {
        try {
            final String email = editTextEmail.getText().toString().trim();
            final String phn = editTextPhone.getText().toString().trim();

            // create bix identity
            String bixIdentity = BixUtil.getBixIdentity(email, phn, "192.168.12.10");

            // init keys
            CryptoUtil.initKeys(this);
            String pubKey = PreferenceUtil.get(this, PreferenceUtil.PUBLIC_KEY);

            // create bix certificate request
            String req = BixUtil.getBixCertificateRequest(bixIdentity, pubKey);
            bixMessage = req;
            write(req);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
    }

    private void navigateNext(Identity identity) {
        Intent intent = new Intent(RegistrationActivity.this, CapturePhotoInfoActivity.class);
        intent.putExtra("IDENTITY", identity);
        RegistrationActivity.this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

    private void navigateToTerms() {
        Intent intent = new Intent(this, TermsOfUseActivity.class);
        startActivity(intent);
    }

    public static final String SENZ_HOST = "96.255.175.56";
    public static final int SENZ_PORT = 23649;

    // senz socket
    private Socket socket;
    private DataInputStream inStream;
    private DataOutputStream outStream;
    private BufferedReader input;

    // comm running
    private boolean running;

    void write(String msg) {
        try {
            //  sends the message to the server
            if (socket != null) {
                Log.e(TAG, "write message --- " + msg);
                outStream.writeBytes(msg + "\n\r");
                outStream.flush();
            } else {
                Log.e(TAG, "Socket disconnected");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SenzCom extends Thread {
        @Override
        public void run() {
            running = true;

            try {
                initCom();
                readCom();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeCom();
            }
        }

        private void initCom() throws IOException {
            Log.d(TAG, "init socket");

            socket = new Socket(InetAddress.getByName(SENZ_HOST), SENZ_PORT);
            inStream = new DataInputStream(socket.getInputStream());
            outStream = new DataOutputStream(socket.getOutputStream());
            //input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }

        private void readCom() throws IOException {
            Log.d(TAG, "read socket");

//            StringBuilder builder = new StringBuilder();
//            int z;
//            char c;
//            while ((z = inStream.read()) != -1) {
//                c = (char) z;
//                System.out.println(c);
//                if (c == '>') {
//                    String resp = builder.toString();
//                    if (!resp.isEmpty()) {
//                        Log.d(TAG, "Response received " + resp);
//
//                        builder = new StringBuilder();
//                    }
//                } else {
//                    builder.append(c);
//                }
//                builder.append(c);
//                if(builder.toString().contains("<end>")) {
//                    Log.d(TAG, "Response received " + builder.toString());
//                }
//            }

//            StringBuilder builder = new StringBuilder();
//            while (true) {
//                final String message = input.readLine();
//                if (message != null) {
//                    builder.append(message);
//                    Log.d(TAG, "response " + message);
//                } else {
//                    Log.d(TAG, "final response " + builder.toString());
//                }
//            }

//            BufferedInputStream inputS = new BufferedInputStream(inStream);
//            byte[] buffer = new byte[5024];    //If you handle larger data use a bigger buffer size
//            int read;
//            while ((read = inputS.read(buffer)) != -1) {
//                System.out.println(read);
//                // Your code to handle the data
//            }
//
//            System.out.println("----" + buffer.length);
//            String o = new String(buffer, Charset.forName("UTF-8"));
//            System.out.println(o);

            int c;
            StringBuilder raw = new StringBuilder();
            do {
                c = inStream.read();
                raw.append((char) c);
            } while (inStream.available() > 0);
            String resp = raw.toString();
            System.out.println("/////" + resp);
        }

        private void closeCom() {
            running = false;

            try {
                if (socket != null) {
                    socket.close();
                    inStream.close();
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onPostReg() {

    }

}
