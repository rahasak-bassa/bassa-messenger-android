package com.rahasak.bix.util;

import com.rahasak.bix.pojo.Account;
import com.rahasak.bix.pojo.Bank;
import com.rahasak.bix.pojo.BankAccount;
import com.rahasak.bix.pojo.BankAccountResponse;
import com.rahasak.bix.pojo.Branch;
import com.rahasak.bix.pojo.ConnectNotificationMessage;
import com.rahasak.bix.pojo.Identity;
import com.rahasak.bix.pojo.NotificationMessage;
import com.rahasak.bix.pojo.StatusReply;
import com.rahasak.bix.pojo.TokenReply;
import com.rahasak.bix.pojo.Transaction;
import com.rahasak.bix.pojo.TransactionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class JsonUtil {

    public static String toJsonContract(HashMap<String, String> contractMap) throws JSONException {
        JSONObject jsonParam = new JSONObject(contractMap);
        return jsonParam.toString();
    }

    public static StatusReply toStatusReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            StatusReply statusReply = new StatusReply();
            statusReply.setCode(jsonObj.getInt("code"));
            statusReply.setMsg(jsonObj.getString("msg"));

            return statusReply;
        }

        throw new JSONException("Invalid JSON");
    }

    public static TokenReply toTokenReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            TokenReply tokenReply = new TokenReply();
            tokenReply.setStatus(jsonObj.getInt("status"));
            tokenReply.setToken(jsonObj.getString("token"));

            return tokenReply;
        }

        throw new JSONException("Invalid JSON");
    }

    public static NotificationMessage toNotificationMessage(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            NotificationMessage msg = new NotificationMessage();
            msg.setPromizeId(jsonObj.getString("promizeId"));
            msg.setPromizeUser(jsonObj.getString("promizeUser"));
            msg.setPromizeAmount(jsonObj.getString("promizeAmount"));
            msg.setPromizeSalt(jsonObj.getString("promizeSalt"));
            msg.setPromizeStatus(jsonObj.getString("promizeStatus"));

            return msg;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ConnectNotificationMessage toConnectNotificationMessage(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            ConnectNotificationMessage msg = new ConnectNotificationMessage();
            msg.setTraceId(jsonObj.getString("traceId"));
            msg.setTracerDid(jsonObj.getString("tracerDid"));
            msg.setTracerOwnerDid(jsonObj.getString("tracerOwnerDid"));
            msg.setTracerName(jsonObj.getString("tracerName"));
            msg.setSalt(jsonObj.getString("salt"));

            return msg;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Account toAccountReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            Account account = new Account();
            account.setId(jsonObj.getString("id"));
            account.setName(jsonObj.getString("name"));
            account.setNic(jsonObj.getString("nic"));
            account.setNo(jsonObj.getString("no"));
            account.setPhone(jsonObj.getString("phone"));

            return account;
        }

        throw new JSONException("Invalid JSON");
    }

    public static TransactionResponse toTransactionResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("transactions");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                transaction.setDate(obj.getString("date"));
                transaction.setDescription(obj.getString("description"));
                transaction.setAmount(obj.getString("amount"));

                transactions.add(transaction);
            }

            return new TransactionResponse(balance, transactions);
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toPromizeResponse(String jsonStr, String acc) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("promizes");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String from = obj.getString("promizeFrom");
                String to = obj.getString("promizeTo");
                String fromAcc = obj.getString("fromAcc");
                String toAcc = obj.getString("toAcc");
                String fromName = obj.getString("promizeFromName");
                String toName = obj.getString("promizeToName");
                String typ = obj.getString("typ");
                String time = obj.getString("timestamp");
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setId(id);
                transaction.setAmount(obj.getString("amount"));
                transaction.setStatus(obj.getString("status"));

                if (typ.equalsIgnoreCase("PAY")) {
                    transaction.setUser(toName);
                    transaction.setAccount(toAcc);
                    transaction.setFrom(fromAcc);
                    transaction.setType("Pay");
                    transaction.setDescription("Payment");
                } else {
                    if (from.equalsIgnoreCase(acc)) {
                        // sent
                        transaction.setUser(toName);
                        transaction.setAccount(toAcc);
                        transaction.setType("Sent");
                        transaction.setFrom(fromAcc);
                        transaction.setDescription("Sent Promize");
                    } else {
                        // received
                        transaction.setUser(fromName);
                        transaction.setAccount(fromAcc);
                        transaction.setType("Received");
                        transaction.setFrom(toAcc);
                        transaction.setDescription("Received Promize");
                    }
                }

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toTransferResponse(String jsonStr, String acc) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("transfers");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String from = obj.getString("fromAcc");
                String to = obj.getString("toAcc");
                String toName = obj.getString("toName");
                String time = obj.getString("timestamp");
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setId(id);
                transaction.setFrom(from);
                transaction.setAccount(to);
                transaction.setUser(toName);
                transaction.setAmount(obj.getString("amount"));
                transaction.setStatus(obj.getString("status"));
                transaction.setBank(obj.getString("toBank"));
                transaction.setBranch(obj.getString("toBranch"));

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

    public static BankAccountResponse toAccountResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray accountsArr = jsonObject.getJSONArray("accounts");

            ArrayList<String> promizes = new ArrayList<>();
            JSONArray promizeArr = jsonObject.getJSONArray("promizes");
            for (int i = promizeArr.length() - 1; i >= 0; i--) {
                promizes.add(promizeArr.getString(i));
            }

            ArrayList<String> transfers = new ArrayList<>();
            JSONArray transferArr = jsonObject.getJSONArray("transfers");
            for (int i = transferArr.length() - 1; i >= 0; i--) {
                transfers.add(transferArr.getString(i));
            }

            ArrayList<BankAccount> accounts = new ArrayList<>();
            for (int i = 0; i < accountsArr.length(); i++) {
                JSONObject obj = accountsArr.getJSONObject(i);
                String no = obj.getString("no");
                String name = obj.getString("name");
                String typ = obj.getString("typ");
                String abal = obj.getString("availableBalance");
                String cbal = obj.getString("currentBalance");
                boolean isPromizeAccount = obj.getBoolean("isPromizeAccount");

                BankAccount account = new BankAccount();
                account.setNo(no);
                account.setName(name);
                account.setTyp(typ);
                account.setAvailableBalance(abal);
                account.setCurrentBalance(cbal);
                accounts.add(account);
                account.setPromizeAccount(isPromizeAccount);
            }

            BankAccountResponse response = new BankAccountResponse();
            response.setAccounts(accounts);
            response.setPromizes(promizes);
            response.setTransfers(transfers);

            return response;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Bank> toBankList(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray banksArr = jsonObject.getJSONArray("OUTRESPONSEDATA");

            ArrayList<Bank> banks = new ArrayList<>();
            for (int i = 0; i < banksArr.length(); i++) {
                JSONObject obj = banksArr.getJSONObject(i);
                String code = obj.getString("OUTBANKCODE");
                String name = obj.getString("OUTBANKNAME");

                // add non empty banks
                if (code != null && name != null && !code.equalsIgnoreCase("") && !name.equalsIgnoreCase("")) {
                    Bank bank = new Bank(code, name);
                    banks.add(bank);
                }
            }

            return banks;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Branch> toBranchList(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray banksArr = jsonObject.getJSONArray("OUTRESPONSEDATA");

            ArrayList<Branch> branches = new ArrayList<>();
            for (int i = 0; i < banksArr.length(); i++) {
                JSONObject obj = banksArr.getJSONObject(i);
                String code = obj.getString("OUTBRANCHCODE");
                String name = obj.getString("OUTBRANCHNAME");

                // add non empty banks
                if (code != null && name != null && !code.equalsIgnoreCase("") && !name.equalsIgnoreCase("")) {
                    String[] br = name.split("/");
                    Branch branch = new Branch(br.length > 1 ? br[1] : br[0], code);
                    branches.add(branch);
                }
            }

            return branches;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Identity toIdentityResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            Identity identity = new Identity();
            identity.setDid(jsonObj.getString("did"));
            identity.setOwner(jsonObj.getString("owner"));
            identity.setName(jsonObj.getString("name"));
            identity.setNic(jsonObj.getString("nic"));
            identity.setDob(jsonObj.getString("dob"));
            identity.setEmail(jsonObj.getString("email"));
            identity.setPhone(jsonObj.getString("phone"));
            identity.setTaxNo(jsonObj.getString("taxNo"));
            identity.setAddress(jsonObj.getString("address"));
            identity.setBlob(jsonObj.getString("blob"));
            identity.setEmployeeName(jsonObj.getString("employeeName"));
            identity.setOccupation(jsonObj.getString("occupation"));
            identity.setEmployeeAddress(jsonObj.getString("employeeAddress"));
            identity.setVerified(jsonObj.getBoolean("verified"));

            return identity;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toActivityResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("traces");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String tracer = obj.getString("accountTracerName");
                String address = obj.getString("accountTracerAddress");
                String verified = obj.getBoolean("verified") ? "Verified" : "Unverified";
                String owner = obj.getString("accountTracerOwner");
                String time = obj.getString("timestamp");

                transaction.setId(id);
                transaction.setUser(tracer);
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setAmount(address);
                transaction.setDescription(owner);

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toConsentResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("consents");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String tracer = obj.getString("accountConsenterName");
                String address = obj.getString("accountConsenterAddress");
                String status = obj.getString("status");
                String time = obj.getString("timestamp");

                transaction.setId(id);
                transaction.setUser(tracer);
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setAmount(address);
                transaction.setDescription(status);

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }
}
