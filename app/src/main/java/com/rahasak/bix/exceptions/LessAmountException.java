package com.rahasak.bix.exceptions;

public class LessAmountException extends Exception {

    @Override
    public String toString() {
        return "invalid amount";
    }

}
