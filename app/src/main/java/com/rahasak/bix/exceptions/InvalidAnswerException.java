package com.rahasak.bix.exceptions;

/**
 * Exception when throw invalid account
 *
 * @author erangaeb@gmail.com (eranga bandara)
 */
public class InvalidAnswerException extends Exception {

    @Override
    public String toString() {
        return "invalid answers";
    }

}
