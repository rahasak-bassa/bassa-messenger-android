package com.rahasak.bix.application;

import android.app.Application;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Application class to hold shared attributes
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class SenzApplication extends Application {

    private static final String TAG = SenzApplication.class.getName();

    private static Timer timer;

    private static boolean onChat = false;

    private static String chatUser = null;

    private static boolean login = false;

    private static boolean refreshWallet = false;

    private static boolean refreshPromize = false;

    private static boolean refreshConsents = false;

    private static boolean refreshSettings = false;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static void startLogoutTimer() {
        Log.i(TAG, "invoke logout timer");

        // cancel timer first
        cancelLogoutTimer();

        // auto logout in 60 minutes (1000 * 60 * 60)
        timer = new Timer();
        LogOutTimerTask logoutTimeTask = new LogOutTimerTask();
        //timer.schedule(logoutTimeTask, 1000 * 60 * 60);
        timer.schedule(logoutTimeTask, 1000 * 60 * 60);
    }

    public static void cancelLogoutTimer() {
        if (timer != null) {
            Log.i(TAG, "cancel logout timer");

            timer.cancel();
            timer = null;
        }
    }

    public static boolean isOnChat() {
        return onChat;
    }

    public static void setOnChat(boolean onChat) {
        SenzApplication.onChat = onChat;
    }

    public static String getChatUser() {
        return chatUser;
    }

    public static void setChatUser(String chatUser) {
        SenzApplication.chatUser = chatUser;
    }

    public static boolean isLogin() {
        return login;
    }

    public static void setLogin(boolean login) {
        SenzApplication.login = login;
    }

    public static boolean isRefreshWallet() {
        return refreshWallet;
    }

    public static void setRefreshWallet(boolean refreshWallet) {
        SenzApplication.refreshWallet = refreshWallet;
    }

    public static boolean isRefreshPromize() {
        return refreshPromize;
    }

    public static void setRefreshPromize(boolean refreshPromize) {
        SenzApplication.refreshPromize = refreshPromize;
    }

    public static boolean isRefreshConsents() {
        return refreshConsents;
    }

    public static void setRefreshConsents(boolean refreshConsents) {
        SenzApplication.refreshConsents = refreshConsents;
    }

    public static boolean isRefreshSettings() {
        return refreshSettings;
    }

    public static void setRefreshSettings(boolean refreshSettings) {
        SenzApplication.refreshSettings = refreshSettings;
    }

    private static class LogOutTimerTask extends TimerTask {
        @Override
        public void run() {
            //redirect user to login screen
            Log.i(TAG, "fire logout timer");
            SenzApplication.login = false;
        }
    }
}
