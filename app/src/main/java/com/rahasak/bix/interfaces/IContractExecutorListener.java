package com.rahasak.bix.interfaces;

import com.rahasak.bix.pojo.Response;

public interface IContractExecutorListener {
    void onFinishTask(String string);
    void onFinishTask(Response response);
}
