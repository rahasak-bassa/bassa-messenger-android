package com.rahasak.bix.enums;

public enum CustomerActionType {
    NEW_CHEQUE,
    CUSTOMER_LIST,
    NEW_MESSAGE,
}
